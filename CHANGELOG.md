# CHANGELOG

## 2024-11-01

* Move to PHP83
* Update documentation

## 2024-04-14

* Update to FileBin `4.0.2`

## 2024-01-21

* Update to FileBin `4.0.1`

## 2022-12-16

* Update docker image packages

## 2022-06-01

* Provide a filebin `4.0.0` version utilizing PHP8

## 2022-01-19

* Provide a filebin `3.6.2` version utilizing PHP8
  * Docker images with PHP8 are suffixed with `-php8`
  * PHP8 versions will be the default for all upcoming releases `> 3.6.2` and no PHP7 version will be maintained anymore

## 2022-01-18

* Updated to filebin `3.6.2`

## 2021-01-30

* Updated docker image to include latest alpine package changes
* Fixed start of `crond` in s6 overlay, included `crontab` file will be run every 15 minutes correctly now
