#
# Build image
#
FROM alpine:3.20 as builder

ARG FB_VERSION
ARG FB_SOURCE_URL="https://github.com/Bluewind/filebin"
ARG FB_CLONE_DEPTH=1

LABEL maintainer="Varakh <varakh@varakh.de>"

RUN mkdir -p /app

WORKDIR /app

RUN echo $FB_VERSION && \
    echo $FB_SOURCE_URL && \
    echo $FB_CLONE_DEPTH

RUN apk add --update --no-cache git && \
    git config --global advice.detachedHead false && \
    git clone --branch ${FB_VERSION} "${FB_SOURCE_URL}" --depth=${FB_CLONE_DEPTH} /app/fb_src && \
    rm -rf /app/fb_src/.git

FROM alpine:3.20
COPY --from=builder /app/fb_src /var/www

LABEL maintainer="Varakh <varakh@varakh.de>" \
    description="filebin" \
    org.opencontainers.image.authors="Varakh" \
    org.opencontainers.image.vendor="Varakh" \
    org.opencontainers.image.title="filebin" \
    org.opencontainers.image.description="filebin" \
    org.opencontainers.image.base.name="alpine:3"

ENV RECONFIGURE=true \
    MIGRATE=true \
    PHP_MEMORY_LIMIT=512M \
    MAX_UPLOAD=1024M \
    PHP_MAX_FILE_UPLOAD=100 \
    PHP_MAX_POST=1024M \
    DB_DSN='' \
    DB_HOST=localhost \
    DB_PORT=5432 \
    DB_DRIVER=postgre \
    DB_NAME=fb \
    DB_USER=fb \
    DB_PASS=fb \
    DB_PREFIX='' \
    DB_PCONNECT='0' \
    DB_DEBUG='1' \
    DB_CHAR_SET=utf8 \
    DB_COLLAT=utf8_bin \
    DB_SWAP_PRE='' \
    DB_ENCRYPT='0' \
    DB_COMPRESS='0' \
    DB_STRICTON='0' \
    DB_SAVE_QUERIES='0' \
    BASE_URL='' \
    INDEX_PAGE='' \
    ENCRYPTION_KEY='' \
    CACHE_BACKEND=dummy \
    EMAIL_FROM='' \
    UPLOAD_MAX_SIZE=1073741824 \
    UPLOAD_MAX_TEXT_SIZE=2097152 \
    UPLOAD_MAX_AGE=432000 \
    ACTIONS_MAX_AGE=86400 \
    SMALL_UPLOAD_SIZE=5120 \
    TARBALL_MAX_SIZE=1073741824 \
    TARBALL_CACHE_TIME=300 \
    MAX_INVITATION_KEYS=3 \
    SMTP_ENABLED=false \
    SMTP_PROTOCOL='smtp' \
    SMTP_HOST='' \
    SMTP_PORT=587 \
    SMTP_CRYPTO='tls' \
    SMTP_USER='' \
    SMTP_PASS=''

ADD src/wait-for.sh /wait-for.sh

# install dependencies
RUN chmod -x /wait-for.sh && \
    apk add --update --no-cache \
        nginx \
        s6 \
        curl \
        python3 \
        py-pygments \
        imagemagick \
        ghostscript \
        msmtp \
        composer \
        php83 \
        php83-fpm \
        php83-intl \
        php83-curl \
        php83-dom \
        php83-pcntl \
        php83-posix \
        php83-session \
        php83-gd \
        php83-exif \
        php83-phar \
        php83-pdo \
        php83-pgsql \
        php83-pdo_pgsql \
        php83-pdo_mysql \
        php83-mysqli \
        php83-fileinfo \
        php83-mbstring \
        php83-ctype \
        php83-ldap \
        php83-pecl-memcached \
        memcached \
        ca-certificates && \
    rm -rf /var/cache/apk/* && \
    apk add gnu-libiconv --update-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ --allow-untrusted && \
    # prepare www dir
    cp -r /var/www/application/config/example/* /var/www/application/config && \
    # set environments
    sed -i "s|;*memory_limit =.*|memory_limit = ${PHP_MEMORY_LIMIT}|i" /etc/php83/php.ini && \
    sed -i "s|;*upload_max_filesize =.*|upload_max_filesize = ${MAX_UPLOAD}|i" /etc/php83/php.ini && \
    sed -i "s|;*max_file_uploads =.*|max_file_uploads = ${PHP_MAX_FILE_UPLOAD}|i" /etc/php83/php.ini && \
    sed -i "s|;*post_max_size =.*|post_max_size = ${PHP_MAX_POST}|i" /etc/php83/php.ini && \
    sed -i 's+.*sendmail_path =.*+sendmail_path = "/usr/bin/msmtp -C /var/www/msmtprc --logfile /var/www/msmtp.log -a filebinmail -t"+' /etc/php83/php.ini && \
    # clean up and permissions
    rm -rf /var/cache/apk/* && \
    chown nobody:nginx -R /var/www && \
    rm /usr/bin/php && \
    ln -s /usr/bin/php83 /usr/bin/php

# Add nginx config
ADD src/filebin.nginx.conf /etc/nginx/nginx.conf

EXPOSE 80

# add templates for replace env variables in the application
ADD src/config/database.php.tpl /var/www/application/config/database.php.tpl
ADD src/config/config-local.php.tpl /var/www/application/config/config-local.php.tpl
ADD src/config/email.php.tpl /var/www/application/config/email.php.tpl
ADD src/crontab /etc/periodic/15min/crontab
ADD src/configure.php /configure.php
ADD src/configure-mail.sh /var/www/configure-mail.sh

# add overlay
ADD src/s6/ /etc/s6/

# expose start
CMD /usr/bin/php /configure.php && exec s6-svscan /etc/s6/
