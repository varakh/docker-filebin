# FileBin 🐋

A docker image for [FileBin](https://git.server-speed.net/users/flo/filebin/) to have it up and running in seconds.

Visit project
on [https://git.myservermanager.com/varakh/docker-filebin](https://git.myservermanager.com/varakh/docker-filebin)
or [Docker Hub](https://hub.docker.com/r/varakh/filebin).

The main git repository is hosted at
_[https://git.myservermanager.com/varakh/docker-filebin](https://git.myservermanager.com/varakh/docker-filebin)_.
Other repositories are mirrors and pull requests, issues, and planning are managed there.

Contributions are very welcome!

## Getting started

There's an example [`docker-compose.yml`](./docker-compose.yml) file to get started quickly. Before, make
yourself familiar with the environment variables which can be set. Defaults should do as a starting point.

_Be sure to read persisting volumes_ section and execute the required command.

Default database is Postgres. Other databases are supported and can be configured via exposed environment variables.
Please refer to the original documentation of the application for further details. PHP modules for MySQL are included in
the image.

After your database and the application docker container is up and running, add a first user by executing a command
within the docker container:

```shell
# docker
docker exec -it filebin_app sh

# podman
podman exec -it filebin_app sh

# inside container
php /var/www/index.php user add_user
```

_Alternatively_ you can also use *Podman* for deployment, e.g. with something similar to:

```shell
# create recommended volumes
podman volume create fb-data-vol
podman volume create fb-db-vol

# create pod, making it available on localhost only
podman pod create -n filebin -p '127.0.0.1:8080:80'

# start database container
podman run -d --name filebin_db \
  --pod filebin 
  -v fb-db-vol:/var/lib/postgresql/data \
  -e POSTGRES_PASSWORD=fb \
  -e POSTGRES_USER=fb \
  -e POSTGRES_DB=fb \
  docker.io/postgres:15

# start application container
# - generate ENCRYPTION_KEY key via 'openssl rand -hex 16'
# - adapt 'BASE_URL' to your public domain
podman run -d --name filebin_app \
  --pod filebin \
  -v fb-data-vol:/var/www/data/uploads \
  -e ENCRYPTION_KEY=<your_32_long_secret> \
  -e BASE_URL=http://localhost:8080 \
  -e DB_HOST=localhost \
  -e DB_PORT=5432 \
  -e DB_NAME=fb \
  -e DB_USER=fb \
  -e DB_PASS=fb \
  docker.io/varakh/filebin:latest
```

### Persisting volumes

> Postgres data should also be persisted. Please look into the above examples or consult Postgres docker image
> documentation.

You'll probably want FileBin's `uploads/` folder to be persistent across container restarts.

Here's an example on how to persist the `data/uploads/` folder of the application with a host bind. You can also use
actual volumes for that.

* Create folder: `mkdir -p ./filebin_data`
* Afterward, adjust permissions so that the folder can be used within the docker
  container: `chown -R 65534:102 <host-mount>` (`nobody:nginx`)
* Reference the folder as a docker volume, e.g. with `./filebin_data:/var/www/data/uploads`

### Cron jobs

Application specific cron jobs are run every 15 minutes.

### Advanced configuration: customize non-exposed configuration variables

If you need to make frequent changes or adapt configuration values which are not exposed as environment variables, you
probably want the `config-local.php` and `database.php` or the entire `config/` folder on the hosts file system.

In order to do so, first _extract_ the current configuration, e.g. by extracting only the required `.php` files or by
extracting the entire `config/` folder. In this example we'll just use entire folder.

```
docker cp filebin_app:/var/www/application/config/ ./filebin_config
chown -R 65534:102 ./filebin_config
```

Add the `./filebin_config` folder as a host bind to the application docker container, e.g.
with `./filebin_config:/var/www/application/config/`

### Available environment variables

Please have a look into `Dockerfile` for available environment variables, they're all exposed there.

All variables to FileBin itself should be self-explaining. You should also be familiar with the `php.ini` variables.
They're only inserted on build, if you like to increase the file limit above the used php variable values of this image,
you'll need to rebuild the docker image.

There are two environment variables introduced by this image:

* `RECONFIGURE`: If all defined environment should be re-applied to the provided `.tpl` files within the image. You
  probably want this to be `1` unless you mounted your `config/` folder on the host
* `MIGRATE`: Calls FileBin database migration every time the container is started and updates dependencies
  via `composer`
* `SMTP_ENABLED`: Set to `true` in order to enable sending mails via an external SMTP server, set to `false` to use
  PHP's internal mailer, see other `SMTP_` variables in the `Dockerfile`

### Setting up a nginx proxy

Be sure to set the environment variable `BASE_URL` to the same where you expose it,
e.g. `BASE_URL=https://fb.domain.tld`.

An example nginx configuration might look like the following.

```
upstream filebin {
    server 127.0.0.1:8080;
}

server {
    listen 80;
    server_name  fb.domain.tld;
    return 301 https://fb.domain.tld$request_uri;
}

server {
    listen 443 ssl;
    server_name  fb.domain.tld;

    ssl_certificate /etc/letsencrypt/live/fb.domain.tld/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/fb.domain.tld/privkey.pem;

    gzip on;
    access_log off;

    location / {
        proxy_redirect off;
        proxy_pass http://filebin;

        proxy_set_header  Host                $http_host;
        proxy_set_header  X-Real-IP           $remote_addr;
        proxy_set_header  X-Forwarded-Ssl     on;
        proxy_set_header  X-Forwarded-For     $proxy_add_x_forwarded_for;
        proxy_set_header  X-Forwarded-Proto   $scheme;
        proxy_set_header  X-Frame-Options     SAMEORIGIN;

        client_body_buffer_size     128k;

        proxy_buffer_size           4k;
        proxy_buffers               4 32k;
        proxy_busy_buffers_size     64k;
        proxy_temp_file_write_size  64k;
    }
}
```

## Updates

Just use a newly released image version. Configuration should be compatible.

## Backup

Backup the host binds for `uploads/` and the database manually.

If you're using the provided `docker-compose.yml` file you probably can do something like the following and afterwards
backup the extracted file from `/tmp` of your host system:

```
docker exec filebin_db bash -c "/usr/bin/pg_dumpall -U fb|gzip -c > /filebin_db.sql.gz";
docker cp filebin_db:/filebin_db.sql.gz /tmp/;
docker exec filebin_db bash -c "rm /filebin_db.sql.gz";
```

## Building

Steps:

* Clone to local `build/` folder which is later added
* Build image
* Push to registry or use locally

Call `local_build.sh` and export `FB_VERSION=$versionTag` before, e.g. `FB_VERSION=3.6.2`.
